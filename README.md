# An Enoplay Currency Provider

This Enoplay currency provider will take a centralized approach to providing a currency to the Enoplay game community.

The Enoplay website will support any currency that's listed by the community. Those currencies are not restricted to the current approach so long as they can be integrated for users to use.

Here is some reasoning as to why we're going the centralized way for now:

| Application Architecture | Examples | Properties |
| --- | --- | --- |
| Decentralized | Bitcoin, Ethereum, Hedera Hashgraph |- Hard to do <br> - Hard to shut down by authorities |
| Centralized | My-personal-wesbite.com |- Easy to do <br> - Easy to shut down by authorities |
| Centralized + Distributed | Youtube.com, Facebook.com | - Easy to shut down by authorities |
| Centralized + Distributed + Open Source | Gitlab.com, itch.io | - Easy to shut down but is easy to start up again by open source community <br> - If shut down, data loss is a setback |
| Centralized + Distributed + Open Source + Data Sharing/Mirroring | ??? | - Data is shared publicly <br> - Hard to shut down by authorities |

The purpose of this diagram is to illustrate that a decentralized or peer-to-peer application is NOT necessary to achieve openness and trustworthiness.

A centralized solution is just as effective if it (1) is open source and (2) publicly shares its data.

1. **Open source** software exposes the processes that underly the "why" a decision is made.
	- Anyone can run the program with little effort.
	- Anyone can read or update the program.
2. **Public data** sharing exposes the "what" that the processes use to come to a decision.
	- Anyone can use the data with their instance of the program.
	- Anyone can read the data.

Trustworthiness is achieved when anyone can use the given process with the public data to achieve the same results as the centralized service.

## Afterword

Yes, all of this means that anyone can run an identical service and has access to the same data.

No, this is not a bad thing because we don't make money by having information that others don't have. We don't make money by using processes that others don't know.

We make money like everyone else on the network:
   - Playing games or
   - Publishing original games or
   - Receiving money from others

If this service is truly useful to people, even if the instance we run is shut down, other people will step up to carry the responsibility of supporting the game community.

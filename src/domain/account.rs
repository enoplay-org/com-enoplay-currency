
pub struct CurrencyAccount {
	pub uid: String,
	pub url_resource_id: String,

	pub currency_amount: f64,

	pub transaction_class: u64,

	pub transaction_host_domain_uid: String,
	pub transaction_host_domain_url: String,
	pub transaction_host_domain_version: String,

	// pub government_host_domain_uid: String,
	// pub government_host_domain_url: String,
	// pub government_host_domain_version: String,

	pub cryptographic_public_key: String,
	pub cryptographic_key_gen_algorithm: String
}

impl CurrencyAccount {
	pub fn new () -> CurrencyAccount {
		let account = CurrencyAccount{
			uid: String::from(""),
			url_resource_id: String::from(""),
			currency_amount: 0.0,
			transaction_class: 0,
			transaction_host_domain_uid: String::from(""),
			transaction_host_domain_url: String::from(""),
			transaction_host_domain_version: String::from(""),
			// government_host_domain_uid: String::from(""),
			// government_host_domain_url: String::from(""),
			// government_host_domain_version: String::from(""),
			cryptographic_public_key: String::from(""),
			cryptographic_key_gen_algorithm: String::from(""),
		};
		account
	}
}

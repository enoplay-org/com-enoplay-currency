
use std::time::Instant;

pub struct CurrencyTransaction {
	pub uid: String,
	pub url_resource_id: String,

	pub recipient_uid: String,
	pub sender_uid: String,

	pub currency_amount: f64,

	pub memo: String,

	pub recipient_previous_currency_transaction_uid: String,
	pub sender_previous_currency_transaction_uid: String,

	pub recipient_previous_account_currency_amount: f64,
	pub sender_previous_account_currency_amount: f64,

	pub recipient_signature: String,
	pub sender_signature: String,
	pub transaction_host_signature: String,

	pub processor_host_domain_uid: String,
	pub processing_status: String,
	pub processing_memo: String,

	pub date_created: Instant,
	pub date_processed: Option<Instant>,

	pub cryptographic_hash: String
}

impl CurrencyTransaction {
	pub fn new () -> CurrencyTransaction {
		let transaction = CurrencyTransaction{
			uid: String::from(""),
			url_resource_id: String::from(""),
			recipient_uid: String::from(""),
			sender_uid: String::from(""),
			currency_amount: 0.0,
			memo: String::from(""),
			recipient_previous_currency_transaction_uid: String::from(""),
			sender_previous_currency_transaction_uid: String::from(""),
			recipient_previous_account_currency_amount: 0.0,
			sender_previous_account_currency_amount: 0.0,
			processor_host_domain_uid: String::now(""),
			processing_status: String::from(""),
			processing_memo: String::from(""),
			date_created: Instant::now(),
			date_processed: None
		};
		transaction
	}
}

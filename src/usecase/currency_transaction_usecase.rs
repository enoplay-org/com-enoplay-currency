
pub struct CurrencyTransactionUsecase {}

impl CurrencyTransactionUsecase {
    pub fn create_transaction (acount_sender_uid: String, acount_recipient_uid: String, currency_amount: f64) {
        //
    }

    pub fn get_transaction (uid: String) {}

    pub fn sign_transaction (account_uid: String, transaction_uid: String) {}

    pub fn get_latest_transaction (account_uid: String) {}
}


pub struct CurrencyAccountUsecase {}

impl CurrencyAccountUsecase {
    pub fn create_account (signed_message: String, cryptographic_public_key: String, cryptographic_key_gen_algorithm: String) {
        // ensure account hasn't already been created with key
        // verify the message was signed by the key
        // if valid, create account with public key
    }

    pub fn get_account (uid: String) {
        // get account
    }
}
